package com.ubitrans.techtest.speed_o_meter.data.exception;

public class NoGPSException extends Throwable {
    public NoGPSException() {
        super();
    }
}
