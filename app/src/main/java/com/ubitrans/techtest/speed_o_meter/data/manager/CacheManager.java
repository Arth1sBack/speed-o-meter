package com.ubitrans.techtest.speed_o_meter.data.manager;

import android.location.Location;

import java.util.List;

public interface CacheManager {
    void saveLocation(Location location);
    Location retrieveLastLocation();
    void addSpeed(Double speed);
    List<Double> speedSet();
    void resetSpeedList();
}
