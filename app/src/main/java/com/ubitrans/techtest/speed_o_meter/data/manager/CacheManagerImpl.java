package com.ubitrans.techtest.speed_o_meter.data.manager;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

public class CacheManagerImpl implements CacheManager {
    private Location savedLocation;
    private List<Double> speedSet;

    public CacheManagerImpl() {
        this.speedSet = new ArrayList<>();
    }

    @Override
    public void saveLocation(Location location) {
        this.savedLocation = location;
    }

    @Override
    public Location retrieveLastLocation() {
        return savedLocation;
    }

    @Override
    public void addSpeed(Double speed) {
        speedSet.add(speed);
    }

    @Override
    public List<Double> speedSet() {
        return speedSet;
    }

    @Override
    public void resetSpeedList() {
        speedSet.clear();
    }
}
