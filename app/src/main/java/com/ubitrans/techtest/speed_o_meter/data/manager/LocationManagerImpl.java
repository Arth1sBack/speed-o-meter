package com.ubitrans.techtest.speed_o_meter.data.manager;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.gms.location.LocationRequest;
import com.patloew.rxlocation.RxLocation;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;


public class LocationManagerImpl implements MyLocationManager {
    private final RxLocation rxLocation;
    private final LocationRequest locationRequest;
    private final LocationManager locationManager;

    private final static int EARTH_RADIUS = 6371;

    public LocationManagerImpl(Context context) {
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        this.rxLocation = new RxLocation(context);

        this.locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500);
    }

    public Observable<Boolean> isGPSEnabled() {
        return Observable.defer(new Callable<ObservableSource<Boolean>>() {
            @Override
            public ObservableSource<Boolean> call() {
                return Observable.just(locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ));
            }
        });
    }

    public Observable<Location> listeningLocationUpdate() throws SecurityException {
        return rxLocation.location().updates(locationRequest);
    }

    public Observable<Double> getSpeedBetweenLocation(Location previousLocation, Location currentLocation) {
        if (previousLocation == null) {
            return Observable.defer(new Callable<ObservableSource<Double>>() {
                @Override
                public ObservableSource<Double> call() {
                    return Observable.just(0.);
                }
            });
        }

        // calcul du delta entre les deux dernières position
        double dLat = Math.toRadians(currentLocation.getLatitude() - previousLocation.getLatitude());
        double dLon = Math.toRadians(currentLocation.getLongitude() - previousLocation.getLongitude());

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) *
                        Math.cos(Math.toRadians(previousLocation.getLatitude())) *
                        Math.cos(Math.toRadians(currentLocation.getLatitude()));

        // transformation du delta en km
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double kmBetweenLocation = EARTH_RADIUS * c;

        // calcul du temps écoulé entre les deux dernières positions en seconde
        double elapsedTime = (currentLocation.getTime() - previousLocation.getTime()) / 1000;

        // évite d'avoir des valeurs de temps trop petite -> vitesse = infini
        elapsedTime = Math.max(0.5, elapsedTime);
        final double finalElapsedTime = elapsedTime;

        return Observable.defer(new Callable<ObservableSource<Double>>() {
            @Override
            public ObservableSource<Double> call() {
                // calcul v = d / t en km/h
                return Observable.just(Math.abs(kmBetweenLocation / finalElapsedTime * 3600));
            }
        });
    }
}
