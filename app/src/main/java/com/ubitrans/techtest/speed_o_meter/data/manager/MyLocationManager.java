package com.ubitrans.techtest.speed_o_meter.data.manager;

import android.location.Location;

import io.reactivex.Observable;

public interface MyLocationManager {
    Observable<Location> listeningLocationUpdate() throws SecurityException;
    Observable<Boolean> isGPSEnabled();
    Observable<Double> getSpeedBetweenLocation(Location previousLocation, Location currentLocation);
}
