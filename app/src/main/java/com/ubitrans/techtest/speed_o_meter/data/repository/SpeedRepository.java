package com.ubitrans.techtest.speed_o_meter.data.repository;

import android.location.Location;

import com.ubitrans.techtest.speed_o_meter.data.exception.NoGPSException;
import com.ubitrans.techtest.speed_o_meter.data.manager.CacheManager;
import com.ubitrans.techtest.speed_o_meter.data.manager.MyLocationManager;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

public class SpeedRepository {
    private final MyLocationManager locationManager;
    private final CacheManager cacheManager;

    public SpeedRepository(MyLocationManager locationManager, CacheManager cacheManager) {
        this.locationManager = locationManager;
        this.cacheManager = cacheManager;
    }

    public Observable<Double> startListeningSpeed() {
        return locationManager.isGPSEnabled().flatMap(new Function<Boolean, Observable<Double>>() {
            @Override
            public Observable<Double> apply(Boolean gpsEnabled){
                if(gpsEnabled) {
                    return locationManager.listeningLocationUpdate()
                        .flatMap(new Function<Location, ObservableSource<Double>>() {
                            @Override
                            public ObservableSource<Double> apply(Location location){
                                Observable<Double> newSpeed = locationManager.getSpeedBetweenLocation(cacheManager.retrieveLastLocation(), location);
                                cacheManager.saveLocation(location);
                                return newSpeed;
                            }
                        }).doOnNext(new Consumer<Double>() {
                                @Override
                                public void accept(Double speed) {
                                    cacheManager.addSpeed(speed);
                                }
                            });
                } else {
                    return Observable.error(new NoGPSException());
                }
            }
        });
    }

    public Observable<Double> getAverageSpeed() {
        return Observable.defer(new Callable<ObservableSource<Double>>() {
            @Override
            public ObservableSource<Double> call() {
                Double sumOfSpeedList = 0.;
                List<Double> recordedSpeedList = cacheManager.speedSet();
                for (Double speed : recordedSpeedList) {
                    sumOfSpeedList += speed;
                }

                return Observable.just(sumOfSpeedList / recordedSpeedList.size());
            }
        }).doOnNext(new Consumer<Double>() {
            @Override
            public void accept(Double averageSpeed){
                cacheManager.resetSpeedList();
            }
        });
    }
}
