package com.ubitrans.techtest.speed_o_meter.presentation;

import android.app.Application;

import com.ubitrans.techtest.speed_o_meter.data.manager.CacheManager;
import com.ubitrans.techtest.speed_o_meter.data.manager.CacheManagerImpl;
import com.ubitrans.techtest.speed_o_meter.data.manager.MyLocationManager;
import com.ubitrans.techtest.speed_o_meter.data.manager.LocationManagerImpl;
import com.ubitrans.techtest.speed_o_meter.data.repository.SpeedRepository;

public class UbiApplication extends Application {
    private static UbiApplication application;
    public static UbiApplication application(){ return application;}

    private SpeedRepository speedRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        initInjection();
    }

    public SpeedRepository getSpeedRepository() {
        return speedRepository;
    }

    private void initInjection() {
        final MyLocationManager locationManager = new LocationManagerImpl(getApplicationContext());
        final CacheManager cacheManager = new CacheManagerImpl();

        this.speedRepository = new SpeedRepository(locationManager, cacheManager);
    }
}
