package com.ubitrans.techtest.speed_o_meter.presentation.composition;

public interface MessagingComponent {
    void displayError(String message);
    void displaySuccess(String message);
}
