package com.ubitrans.techtest.speed_o_meter.presentation.composition;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.support.design.widget.Snackbar;

public class MessagingComponentSnackbarImpl implements MessagingComponent {
    private final View parentView;
    private final Context context;

    public MessagingComponentSnackbarImpl(View parentView, Context context) {
        this.parentView = parentView;
        this.context = context;
    }

    @Override
    public void displayError(String message){
        Snackbar snackbar = Snackbar.make(parentView, message, Snackbar.LENGTH_SHORT);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.getView().setBackgroundColor(context.getResources().getColor(android.R.color.holo_red_light));
        snackbar.show();
    }

    @Override
    public void displaySuccess(String message) {
        Snackbar snackbar = Snackbar.make(parentView, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.getView().setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_dark));
        snackbar.show();
    }
}
