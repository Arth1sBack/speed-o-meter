package com.ubitrans.techtest.speed_o_meter.presentation.navigator;


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.ubitrans.techtest.speed_o_meter.R;
import com.ubitrans.techtest.speed_o_meter.presentation.ui.fragment.SpeedFragment;
import com.ubitrans.techtest.speed_o_meter.presentation.view.SpeedView;

public class MainNavigator {

    private final Activity activity;
    private final FragmentManager fragmentManager;

    public MainNavigator(FragmentActivity activity) {
        this.activity = activity;
        this.fragmentManager = activity.getSupportFragmentManager();
    }

    public SpeedView displaySpeedFragment() {
        SpeedFragment speedView = SpeedFragment.newInstance();
        fragmentTransaction(speedView);
        return speedView;
    }

    public void dismissSpeedFragment() {
        fragmentManager.popBackStack();
    }

    private void fragmentTransaction(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.activity_main_framelayout, fragment, fragment.getTag());
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }
}
