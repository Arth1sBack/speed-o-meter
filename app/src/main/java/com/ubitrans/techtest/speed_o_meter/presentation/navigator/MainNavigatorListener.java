package com.ubitrans.techtest.speed_o_meter.presentation.navigator;


public interface MainNavigatorListener {
    void displaySpeedFragment();
    void dismissSpeedFragment();
}
