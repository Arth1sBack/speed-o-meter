package com.ubitrans.techtest.speed_o_meter.presentation.presenter;

import android.content.Context;

import com.ubitrans.techtest.speed_o_meter.R;
import com.ubitrans.techtest.speed_o_meter.data.exception.NoGPSException;
import com.ubitrans.techtest.speed_o_meter.data.repository.SpeedRepository;
import com.ubitrans.techtest.speed_o_meter.presentation.navigator.MainNavigatorListener;
import com.ubitrans.techtest.speed_o_meter.presentation.view.MainView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter {
    private final MainView mainView;
    private final SpeedRepository speedRepository;
    private final MainNavigatorListener mainNavigatorListener;
    private final Context context;

    public MainPresenter(MainView mainView, SpeedRepository speedRepository, MainNavigatorListener mainNavigatorListener, Context context) {
        this.mainView = mainView;
        this.speedRepository = speedRepository;
        this.mainNavigatorListener = mainNavigatorListener;
        this.context = context;
    }

    public void startListeningSpeed() {
        speedRepository.startListeningSpeed()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Double> () {
                    @Override
                    public void onNext(Double speed) {
                        if (speed > 1) {
                            mainNavigatorListener.displaySpeedFragment();
                            mainView.updateDisplayedSpeed(speed);
                        } else {
                            mainNavigatorListener.dismissSpeedFragment();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    public void retrieveAverageSpeed() {
        speedRepository.getAverageSpeed()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Double>() {
                    @Override
                    public void onNext(Double averageSpeed) {
                        if(averageSpeed > 0) {
                            mainView.displayAverageSpeed(averageSpeed);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    private void handleError(Throwable error) {
        if (error instanceof NoGPSException) {
            mainView.displayError(context.getString(R.string.error_gps_not_enabled));
            return;
        }
        // autres erreurs
        mainView.displayError(error.getMessage());
    }
}
