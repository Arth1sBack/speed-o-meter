package com.ubitrans.techtest.speed_o_meter.presentation.ui.activity;

import android.Manifest;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.ubitrans.techtest.speed_o_meter.R;
import com.ubitrans.techtest.speed_o_meter.presentation.UbiApplication;
import com.ubitrans.techtest.speed_o_meter.presentation.composition.MessagingComponent;
import com.ubitrans.techtest.speed_o_meter.presentation.composition.MessagingComponentSnackbarImpl;
import com.ubitrans.techtest.speed_o_meter.presentation.navigator.MainNavigator;
import com.ubitrans.techtest.speed_o_meter.presentation.navigator.MainNavigatorListener;
import com.ubitrans.techtest.speed_o_meter.presentation.presenter.MainPresenter;
import com.ubitrans.techtest.speed_o_meter.presentation.view.MainView;
import com.ubitrans.techtest.speed_o_meter.presentation.view.SpeedView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends AppCompatActivity implements MainView, MainNavigatorListener {

    @BindView(R.id.activity_main_textview_speed)
    TextView textViewAverageSpeed;
    @BindView(R.id.activity_main_contraintlayout)
    ConstraintLayout constraintLayout;

    private MainPresenter mainPresenter;
    private MainNavigator mainNavigator;
    private MessagingComponent messagingComponent;

    private SpeedView speedView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initializeInjection();

        MainActivityPermissionsDispatcher.startListeningSpeedWithPermissionCheck(this);

        textViewAverageSpeed.setText(R.string.activity_main_no_recorded_average);
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void startListeningSpeed() {
        mainPresenter.startListeningSpeed();
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForLocation() {
        displayError(getString(R.string.error_location_permission_not_granted));
    }

    /* MainNavigatorListener implementation */

    @Override
    public void displaySpeedFragment() {
        if(speedView == null) {
            speedView = mainNavigator.displaySpeedFragment();
        }
    }

    @Override
    public void dismissSpeedFragment() {
        if(speedView != null) {
            mainNavigator.dismissSpeedFragment();
            speedView = null;
            mainPresenter.retrieveAverageSpeed();
        }
    }

    /* MainView implementation */

    @Override
    public void updateDisplayedSpeed(Double speed) {
        if (speedView != null) {
            speedView.updateSpeed(speed);
        }
    }

    @Override
    public void displayAverageSpeed(Double speed) {
        String formatedSpeed = new DecimalFormat("#").format(speed);
        String baseString = getString(R.string.activity_main_last_average_speed);
        textViewAverageSpeed.setText(String.format(baseString, formatedSpeed));
    }

    @Override
    public void displayError(String message) {
        messagingComponent.displayError(message);
    }

    /* Permission implementation */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void initializeInjection() {
        mainPresenter = new MainPresenter(this, UbiApplication.application().getSpeedRepository(),this, this);
        mainNavigator = new MainNavigator(this);
        messagingComponent = new MessagingComponentSnackbarImpl(constraintLayout, this);
    }
}
