package com.ubitrans.techtest.speed_o_meter.presentation.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ubitrans.techtest.speed_o_meter.R;
import com.ubitrans.techtest.speed_o_meter.presentation.view.SpeedView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpeedFragment extends Fragment implements SpeedView {

    @BindView(R.id.fragment_speed_textview_speed)
    TextView textViewSpeed;

    public static SpeedFragment newInstance() {
        return new SpeedFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_speed, container,false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void updateSpeed(double speed) {
        if(textViewSpeed != null){
            String formatedSpeed = new DecimalFormat("#").format(speed);
            textViewSpeed.setText(String.format(getActivity().getString(R.string.fragment_speed_displayedSpeed), formatedSpeed));
        }
    }
}
