package com.ubitrans.techtest.speed_o_meter.presentation.view;

public interface MainView {
    void updateDisplayedSpeed(Double speed);
    void displayAverageSpeed(Double speed);
    void displayError(String message);
}
