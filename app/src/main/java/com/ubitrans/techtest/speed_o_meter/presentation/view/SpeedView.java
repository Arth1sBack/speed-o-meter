package com.ubitrans.techtest.speed_o_meter.presentation.view;

public interface SpeedView {
    void updateSpeed(double speed);
}
